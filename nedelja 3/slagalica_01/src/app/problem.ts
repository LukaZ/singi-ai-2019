import {Stanje} from './stanje';


export class Problem{

    isGoal(stanje:Stanje):boolean{
        let s = stanje.h();
        if(s == 9)
            return true;
        else
            return false;
    }


    sledecaStanja(stanje:Stanje):Stanje[]{
        let smerovi = [
            [-1, 0], [0, -1], [1, 0], [0, 1]
        ];
        let [pi, pj] = stanje.prazno();
        let retVal = [];

        for(let k in smerovi){
            let [di, dj] = smerovi[k];
            let ni = pi+di;
            let nj = pj+dj;
            if(ni>-1 && ni<3 && nj>-1 && nj<3){
                let kopija = stanje.copy();
                kopija.onClick(ni, nj);
                retVal.push(kopija);
            }
        }
        return retVal;
    }


}