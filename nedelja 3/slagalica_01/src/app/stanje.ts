import { readElementValue } from '@angular/core/src/render3/util';

export class Stanje {

    tabla: number[][];

    constructor() {
        this.tabla = [];
        let c = 1;
        for (let i = 0; i < 3; i++) {
            // this.tabla.push([]);
            this.tabla[i] = [];
            for (let j = 0; j < 3; j++) {
                // this.tabla[i].push(c);
                this.tabla[i][j] = c;
                c++;
            }
        }
    }

    prazno(): [number, number] {
        for (let i = 0; i < 3; i++)
            for (let j = 0; j < 3; j++) {
                if (this.tabla[i][j] == 9) {
                    return [i, j];
                }
            }
        return [-1, -1];
    }

    onClick(ni: number, nj: number): void {
    // 
    //

        let [pi, pj] = this.prazno();
        let s = Math.abs(ni - pi) + Math.abs(nj - pj);
        if (s == 1) {
            this.tabla[pi][pj] = this.tabla[ni][nj];
            this.tabla[ni][nj] = 9;
        }
    }

    h():number{
        let retVal = 0;
        let c = 1;
        for(let i=0; i<3; i++){
            for(let j=0; j<3; j++){
                if(this.tabla[i][j] == c){
                    retVal++;
                }
                c++;
            }
        }
        return retVal;
    }


    copy():Stanje{
        let retVal:Stanje = new Stanje();
        for(let i=0; i<3; i++){
            for(let j=0; j<3; j++){
                retVal.tabla[i][j] = this.tabla[i][j];
            }
        }
        return retVal;
    }

    equals(stanje:Stanje):boolean{
        for(let i=0; i<3; i++){
            for(let j=0; j<3; j++){
                if(this.tabla[i][j] != stanje.tabla[i][j]){
                    return false;
                }
            }
        }
        console.log("ISTI");
        return true;
    }

}