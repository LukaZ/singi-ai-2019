import json
from PySide2 import QtWidgets
from PySide2 import QtGui


class MazeModel(QtWidgets.QGraphicsScene):
    tiles = {}

    def __init__(self, size_x=15, size_y=15):
        super().__init__()
        self.size_x = size_x
        self.size_y = size_y
        self._items = {}
        self.maze = []
        self.editing = False
        MazeModel._load_tiles()
        self.populate_matrix()

    def populate_matrix(self):
        for i in range(self.size_y):
            self.maze.append([])
            for j in range(self.size_x):
                self.maze[i].append("floor")
                self._items[(j, i)] = QtWidgets.QGraphicsPixmapItem(MazeModel.tiles["floor"]["tile"])
                self._items[(j, i)].setPos(j*64, i*64)
                self.addItem(self._items[(j, i)])

    def set_value(self, x, y, value):
        self.maze[y][x] = value
        self._update_scene(x, y)

    def get_tile(self, x, y):
        return MazeModel.tiles[self.maze[y][x]]

    def get_tiles_by_type(self, tile_type):
        found = []
        for y in range(self.size_y):
            for x in range(self.size_x):
                if self.maze[y][x] == tile_type:
                    tile = {"x": x, "y": y}
                    tile.update(MazeModel.tiles[tile_type])
                    found.append(tile)
        return found

    @staticmethod
    def read(path):
        with open(path) as fp:
            maze = json.load(fp)
            maze_model = MazeModel(len(maze), len(maze[0]))
            maze_model.maze = maze
            maze_model._update_scene()
            return maze_model

    def write(self, path):
        with open(path, "w") as fp:
            json.dump(self.maze, fp)

    @staticmethod
    def _tint_pixmap(painter, pixmap, color):
        painter.begin(pixmap)
        painter.setCompositionMode(QtGui.QPainter.CompositionMode_Multiply)
        if pixmap.hasAlpha():
            mask = pixmap.mask()
            painter.setClipRegion(QtGui.QRegion(mask))
        painter.fillRect(pixmap.rect(), QtGui.QColor(color))
        painter.end()

    @staticmethod
    def _load_tiles():
        pixmap_painter = QtGui.QPainter()
        if MazeModel.tiles == {}:
            with open("resources/tiles/tiles.json") as fp:
                MazeModel.tiles = json.load(fp)
                for k in MazeModel.tiles:
                    MazeModel.tiles[k]["tile"] = QtGui.QPixmap(MazeModel.tiles[k]["tile"])
                    if MazeModel.tiles[k].get("tile_tint") is not None:
                        MazeModel._tint_pixmap(pixmap_painter, MazeModel.tiles[k]["tile"], MazeModel.tiles[k]["tile_tint"])
                    if MazeModel.tiles[k].get("item") is not None:
                        item = QtGui.QPixmap(MazeModel.tiles[k]["item"])
                        if MazeModel.tiles[k].get("item_tint") is not None:
                            MazeModel._tint_pixmap(pixmap_painter, item, MazeModel.tiles[k]["item_tint"])

                        pixmap_painter.begin(MazeModel.tiles[k]["tile"])
                        pixmap_painter.setCompositionMode(QtGui.QPainter.CompositionMode_SourceOver)
                        pixmap_painter.drawPixmap(0, 0, item)
                        pixmap_painter.end()

                    if MazeModel.tiles[k].get("tint") is not None:
                        MazeModel._tint_pixmap(pixmap_painter, MazeModel.tiles[k]["tile"], MazeModel.tiles[k]["tint"])

    def _update_scene(self, x=None, y=None):
        if x is not None and y is not None:
            self._items[(x, y)].setPixmap(MazeModel.tiles[self.maze[y][x]]["tile"])
        elif x is None and y is not None:
            pass
        elif y is None and x is not None:
            pass
        else:
            for y in range(self.size_y):
                for x in range(self.size_x):
                    self._items[(x, y)].setPixmap(MazeModel.tiles[self.maze[y][x]]["tile"])
