class State:
    def __init__(self, x, y, tile, parent_state=None):
        self.x = x
        self.y = y
        self.tile = tile
        self.parent_state = parent_state

    def get_next_states(self, maze_model):
        next_states = []
        return next_states

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))
