import math
from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtGui
from ..model.maze_model import MazeModel


class MazeEditor(QtWidgets.QGraphicsView):
    def __init__(self, parent, model):
        super().__init__(parent=parent)
        self.model = None
        self.current_tile = None
        self.animation = None
        self.editing = False
        self.setModel(model)

        self.actions = {
            "new": QtWidgets.QAction(QtGui.QIcon("resources/icons/document--plus.png"), "New"),
            "open": QtWidgets.QAction(QtGui.QIcon("resources/icons/folder-open-document.png"), "Open"),
            "save": QtWidgets.QAction(QtGui.QIcon("resources/icons/disk-return-black.png"), "Save")
        }

        self.tiles_palette = QtWidgets.QListWidget()
        self.tiles_palette.setIconSize(QtCore.QSize(64, 64))

        self._bind_actions()
        self._populate_tile_palette()

    def setModel(self, model):
        self.model = model
        if self.animation is not None:
            self.animation.stop_animation()
            self.animation = None
        self.setScene(model)

    def set_animation(self, animation):
        if self.animation is not None:
            self.animation.stop_animation()
            self.model.removeItem(self.animation)
        if animation is not None:
            self.animation = animation
            self.model.addItem(self.animation)

    def run_animation(self):
        if self.animation is not None:
            self.animation.run_animation()

    def read(self):
        path = QtWidgets.QFileDialog.getOpenFileName(self, "Save maze", None, "JSON (*.json)")
        if path is not None:
            self.setModel(MazeModel.read(path[0]))

    def write(self):
        path = QtWidgets.QFileDialog.getSaveFileName(self, "Save maze", None, "JSON (*.json)")
        if path is not None:
            self.model.write(path[0])

    def mousePressEvent(self, event):
        if event.button() == 1:
            self.editing = True
            point = self._get_tile_coordinate(self.mapToScene(event.pos()))
            self.paint_tile(point, self._selected_tile())
        super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        self.editing = False
        super().mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        if self.editing:
            point = self._get_tile_coordinate(self.mapToScene(event.pos()))
            self.paint_tile(point, self._selected_tile())
        super().mouseMoveEvent(event)

    def _bind_actions(self):
        self.actions["open"].triggered.connect(self.read)
        self.actions["save"].triggered.connect(self.write)

    def _populate_tile_palette(self):
        for k, v in self.model.tiles.items():
            item = QtWidgets.QListWidgetItem(QtGui.QIcon(v["tile"]), v["title"])
            item.setData(QtCore.Qt.UserRole, k)
            self.tiles_palette.addItem(item)

    def _selected_tile(self):
        current_item = self.tiles_palette.currentItem()
        if current_item is None:
            return None

        return current_item.data(QtCore.Qt.UserRole)

    def _get_tile_coordinate(self, pos):
        x = math.floor(pos.x()/64)
        y = math.floor(pos.y()/64)
        if x >= 0 and x < self.model.size_x and y >= 0 and y < self.model.size_y:
            return(int(x), int(y))
        return None

    def paint_tile(self, point, tile):
        if point is None or tile is None:
            return

        x, y = point
        self.model.set_value(x, y, tile)
