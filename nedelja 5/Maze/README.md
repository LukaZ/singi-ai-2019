# Maze - graph search algorithm playground

## Installation

In activated Python 3.7 virtual env run:

```python
python -m pip install -r requirements.txt
```

## Usage

In activated Python 3.7 virtual env run:

```python
python main.py
```