import random
from .state import State


class Solver:
    def __init__(self, maze, algorithm):
        self.maze = maze
        self.algorithm = algorithm

    def get_path(self, final_state):
        path = []
        current_state = final_state
        while current_state is not None:
            path.append(current_state)
            current_state = current_state.parent_state
        path.reverse()
        return path

    def solve(self):
        start_tile = random.sample(self.maze.get_tiles_by_type("entrance"), 1)[0]
        start_state = State(start_tile["x"], start_tile["y"], start_tile, None)

        # Primer pronalazenja dva kljuca pre izlaza.
        sakupljeni_kljucevi = []  # Trenutno pronadjeni kljucevi

        res = self.algorithm(start_state, self.maze, "key", sakupljeni_kljucevi)  # Pretraga za prvim kljucem
        # Promenljiva res je torka (konacno_stanje, posecena_stanja)
        sakupljeni_kljucevi.append(res[0])  # Dodat novi pronadjeni kljuc
        res2 = self.algorithm(res[0], self.maze, "key", sakupljeni_kljucevi)  # Pretraga za drugim kljucem
        res3 = self.algorithm(res2[0], self.maze, "exit", [])  # Pretraga za izlazom

        res[1].extend(res2[1])  # Na posecena stanja prve pretrage dodaju se posecena stanja druge pretrage
        res[1].extend(res3[1])  # Na posecena stanja prve pretrage dodaju se posecena stanja trece pretrage
        res = (self.get_path(res3[0]), res[1])  # Konacan rezultat je torka (putanja_do_cilja, sva_posecena_stanja)

        return res
