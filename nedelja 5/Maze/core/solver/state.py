class State:
    def __init__(self, x, y, tile, parent_state=None):
        self.x = x
        self.y = y
        self.tile = tile
        self.parent_state = parent_state
        self.cost = 1
        if tile.get("cost") is not None:
            self.cost += tile.get("cost")
        if parent_state is not None:
            self.cost += parent_state.cost

        self.total_cost = self.cost  # Cena u koju je uracunata heuristika

    def get_next_states(self, maze_model):
        next_states = []
        if self.x+1 < maze_model.size_x and maze_model.get_tile(self.x+1, self.y).get("passable"):
            next_states.append(State(self.x+1, self.y, maze_model.get_tile(self.x+1, self.y), self))
            next_states[-1].total_cost += next_states[-1].h(maze_model)  # Ukupna cena je cena + rezultat f-je h
        if self.x-1 >= 0 and maze_model.get_tile(self.x-1, self.y).get("passable"):
            next_states.append(State(self.x-1, self.y, maze_model.get_tile(self.x-1, self.y), self))
            next_states[-1].total_cost += next_states[-1].h(maze_model)
        if self.y+1 < maze_model.size_y and maze_model.get_tile(self.x, self.y+1).get("passable"):
            next_states.append(State(self.x, self.y+1, maze_model.get_tile(self.x, self.y+1), self))
            next_states[-1].total_cost += next_states[-1].h(maze_model)
        if self.y-1 >= 0 and maze_model.get_tile(self.x, self.y-1).get("passable"):
            next_states.append(State(self.x, self.y-1, maze_model.get_tile(self.x, self.y-1), self))
            next_states[-1].total_cost += next_states[-1].h(maze_model)
        return next_states

    def h(self, maze_model):
        tiles = maze_model.get_tiles_by_type("exit")
        tile = min(tiles, key=lambda t: abs(t["x"]-self.x) + abs(t["y"]-self.y))
        return abs(tile["x"]-self.x) + abs(tile["y"]-self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))
