import random
import math
import numpy as np
import matplotlib.pyplot as plt

dataset1 = {
    "x": [i/10+random.random()*10 for i in range(1000)],
    "y": [i/10+random.random()*10 for i in range(1000)]
}

dataset2 = {
    "x": [i/10+random.random()*10 for i in range(1000)],
    "y": [((i/10)**2+random.random()*10)/100 for i in range(1000)]
}

dataset3 = {
    "x": [i/10 for i in range(1000)],
    "y": [((i/10)**2 + math.sin(i/50)*1000 + random.random()*1000)/100 for i in range(1000)]
}

def simple_linear_regression(x, y):
    # y = ax + b
    # a = (n*sum(xi*yi) - sum(xi)sum(yi))/(n(sum(xi^2)-sum(xi)^2))
    # b = (1/n)*(sum(yi) - a*sum(xi))
    n = len(x)
    xy = map(lambda p: p[0]*p[1], zip(x, y))
    sx = sum(x)
    sy = sum(y)
    sx2 = sum(map(lambda x: x**2, x))

    a = (n*xy - sx*sy)/(n*sx2 - sx**2)
    b = (sy - a*sx)/n

    return a, b


def least_squares_linear_regression(x, y):
    # y = x*a
    # a = y/x
    pass

# a, b = simple_linear_regression(dataset1["x"], dataset1["y"])
# res = least_squares_linear_regression(dataset1["x"], dataset1["y"])
# print(a, b)
# print(res)

plt.scatter(dataset1["x"], dataset1["y"])
plt.scatter(dataset2["x"], dataset2["y"])
plt.scatter(dataset3["x"], dataset3["y"])

x = [i/10 for i in range(0, 1000)]
plt.show()